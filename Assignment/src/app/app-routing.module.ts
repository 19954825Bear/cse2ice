import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ParentComponent } from './parent/parent.component';
import { RSSFeedComponent } from './rss-feed/rss-feed.component';
import { LadderComponent } from './ladder/ladder.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { Head2HeadComponent } from './head2-head/head2-head.component';

const routes: Routes = [

  { path: '', component: ParentComponent },
  { path: 'rssfeed', component: RSSFeedComponent },
  { path: 'ladderComponent', component: LadderComponent },
  { path: 'statisticsComponent', component: StatisticsComponent },
  { path: 'head2headComponent', component: Head2HeadComponent }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
