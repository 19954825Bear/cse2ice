import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { DemoMaterialModule } from './material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { RSSFeedComponent } from './rss-feed/rss-feed.component';
import { LadderComponent } from './ladder/ladder.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { Head2HeadComponent } from './head2-head/head2-head.component';
import { QueryServices } from './query-services';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { MatTableModule } from '@angular/material';
import { MatButtonModule, MatRippleModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    ParentComponent,
    ChildComponent,
    RSSFeedComponent,
    LadderComponent,
    StatisticsComponent,
    Head2HeadComponent,
  ],
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
  ],
  exports: [
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
  ],
  providers: [QueryServices],
  bootstrap: [AppComponent]
})

export class AppModule { }
