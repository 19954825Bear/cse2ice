export class ladderQuery {
    constructor(
        public mean_rank: number,
        public teamid: number,
        public percentage: number,
        public swarms: Array<number>,
        public updated: string,
        public year: number,
        public team: string,
        public source: string,
        public round: number,
        public rank: number,
        public sourceid: number,
        public wins: number
    ) { }
}