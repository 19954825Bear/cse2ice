import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { QueryServices } from '../query-services';
import { Team } from '../team';
import { DataQuery } from '../data-query';
import { MatTableModule } from '@angular/material/table';
import { MatTableDataSource } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {

  // exampleDatabase: ExampleDatabase | null;
  // data: SquiggleData[] = [];

  // ELEMENT_DATA: DataQuery[];

  // displayedColumns: string[] = ['round','hteam','ateam','hscore','ascore','hgoals','agoals','hbehinds','abehinds','winner','date','venue'];
  // dataSource = new MatTableDataSource(this.data);

  teams: Team[];
  weeks: DataQuery[];
  nweeks: DataQuery[];

  constructor(private queryServices: QueryServices) { }
  ngOnInit() {
    this.getAllTeams();
    this.getDataQuery();
    this.getDataQueryTwo();
    // this.getFilterQuery();
  }
  getAllTeams(): void {
    this.queryServices.getTeams().subscribe(temp => { this.teams = temp; });
  }
  getDataQuery(): void {
    this.queryServices.getDataQuery().subscribe(temp => { this.weeks = temp; });
  }
  getDataQueryTwo(): void {
    this.queryServices.getDataQueryTwo().subscribe(temp => { this.nweeks = temp; });
  }
  // getFilterQuery(): void{
  //   this.queryServices.getDataQuery().subscribe(temp => {this.dataSource.data = temp;});
  // }
}
// export interface SquiggleAPI {
//   items: SquiggleData[];
// }
// export interface SquiggleData{
//          complete: number,
//          is_grand_final: number, 
//          tz:string,
//          hbehinds: number,
//          ateam: string,
//          winnerteamid: number,
//          hgoals: number,
//          updated: string,
//          round: number, 
//          is_final: number,
//          hscore: number,
//          abehinds: number,
//          winner: string,
//          ascore: number,
//          hteam: string,
//          ateamid: number,
//          venue: string,
//          hteamid: number,
//          agoals: number,
//          year: number,
//          date: string,
//          id: number
// }
// export class ExampleDatabase{ constructor(private _httpClient:HttpClient) {}
//   getSquiggleShibang():Observable<SquiggleAPI>{
//     const href = 'https://api.squiggle.com.au';
//     const requestUrl = '$(href)/?q=games;year=2019;complete=100';
//     return this._httpClient.get<SquiggleAPI>(requestUrl);
// }

