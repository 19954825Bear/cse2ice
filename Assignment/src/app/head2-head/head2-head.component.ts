import { Component, OnInit } from '@angular/core';

import { QueryServices } from '../query-services';
import { Team } from '../team';
import { DataQuery } from '../data-query';



import { ladderQuery } from '../ladderQuery';
import { CompileMetadataResolver } from '@angular/compiler';

@Component({
  selector: 'app-head2-head',
  templateUrl: './head2-head.component.html',
  styleUrls: ['./head2-head.component.css']
})
export class Head2HeadComponent implements OnInit {

  teams: Team[];
  weeks: DataQuery[];

  constructor(private queryServices: QueryServices) {
  }

  ngOnInit() {
    this.getTeams();
    this.getDataQueryThree();
  }

  getTeams(): void {
    this.queryServices.getTeams().subscribe(temp => { this.teams = temp; });

  }
  getDataQueryThree(): void {
    this.queryServices.getDataQueryThree().subscribe(temp => { this.weeks = temp; });

  }

}
