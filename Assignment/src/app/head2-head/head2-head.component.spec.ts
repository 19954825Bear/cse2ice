import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Head2HeadComponent } from './head2-head.component';

describe('Head2HeadComponent', () => {
  let component: Head2HeadComponent;
  let fixture: ComponentFixture<Head2HeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Head2HeadComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Head2HeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
