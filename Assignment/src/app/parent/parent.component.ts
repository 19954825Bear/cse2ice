import { Component, OnInit } from '@angular/core';
import { QueryServices } from '../query-services';
import { Team } from '../team';
import { DataQuery } from '../data-query';
import { Tips } from '../tips';
import { ladderQuery } from '../ladderQuery';
import { CompileMetadataResolver } from '@angular/compiler';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

  tips: Tips[];
  teams: Team[];
  weeks: DataQuery[];

  constructor(private queryServices: QueryServices) {
  }

  ngOnInit() {
    this.getTeams();
    this.getDataQueryThree();
  }

  getTeams(): void {
    this.queryServices.getTeams().subscribe(temp => { this.teams = temp; });
  }
  getDataQueryThree(): void {
    this.queryServices.getDataQueryThree().subscribe(temp => { this.weeks = temp; });
  }
  getTipsQuery(): void {
    this.queryServices.getTips().subscribe(temp => { this.tips = temp; });
  }
}
