export class Tips {
    constructor(
        public venue: String,
        public round: Number,
        public hteamid: Number,
        public hteam: String,
        public tipteamid: Number,
        public sourceid: Number,
        public updated: String,
        public hconfidence: Number,
        public bits: Number,
        public ateamid: Number,
        public ateam: String,
        public year: Number,
        public margin: Number,
        public gameid: Number,
    ){}
}
