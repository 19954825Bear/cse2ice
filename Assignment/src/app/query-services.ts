import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {Team} from './team';
import {DataQuery} from './data-query';
import {ladderQuery} from'./ladderQuery';
import {Tips} from './tips';


@Injectable({
    providedIn: 'root'
    
  })

export class QueryServices {
    constructor(private http: HttpClient) { }
  
  

    getTeams() : Observable<Team[]> {
      return this.http.get('https://api.squiggle.com.au/?q=teams').pipe(
        map((data: any) => data.teams.map((item: any) => new Team(
          item.logo,
          item.id,
          item.name,
          item.abbrev
        )))
      );
    }
    getTips() : Observable<Tips[]>{
      return this.http.get('https://api.squiggle.com.au/?q=tips;round=11;year=2019').pipe(
        map((data: any) => data.tips.map((item: any) => new Tips(
            item.venue,
            item.round,
            item.hteamid,
            item.hteam,
            item.tipteamid,
            item.sourceid,
            item.updated,
            item.hconfidence,
            item.bits,
            item.ateamid,
            item.ateam,
            item.year,
            item.margin,
            item.gameid
        )))
      );

    }

    getDataQuery() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;complete=100').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getDataQueryTwo() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;complete=0').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }
    getDataQueryThree() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=11').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }
    getLadderQueryRnd1() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=1;complete=100').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }
    getLadderQueryRnd2() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=2;complete=100').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }
    getLadderQueryRnd3() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=3;complete=100').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }
    getLadderQueryRnd4() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=4;complete=100').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }
    getLadderQuery() : Observable<ladderQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=ladder;year=2019;round=5;source=17').pipe(
        map((data: any) => data.ladder.map((item: any) => new ladderQuery(
          item.mean_rank,
          item.teamid,
          item.percentage,
          item.swarns,
          item.updated,
          item.year,
          item.team,
          item.source,
          item.round,
          item.rank,
          item.sourceid,
          item.wins
        )))
      ); 
    }
    getLadderQueryRnd6() : Observable<ladderQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=ladder;year=2019;round=6;source=17').pipe(
        map((data: any) => data.ladder.map((item: any) => new ladderQuery(
          item.mean_rank,
          item.teamid,
          item.percentage,
          item.swarns,
          item.updated,
          item.year,
          item.team,
          item.source,
          item.round,
          item.rank,
          item.sourceid,
          item.wins
        )))
      ); 
    }
    getLadderQueryRnd7() : Observable<ladderQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=ladder;year=2019;round=7;source=17').pipe(
        map((data: any) => data.ladder.map((item: any) => new ladderQuery(
          item.mean_rank,
          item.teamid,
          item.percentage,
          item.swarns,
          item.updated,
          item.year,
          item.team,
          item.source,
          item.round,
          item.rank,
          item.sourceid,
          item.wins
        )))
      ); 
    }
    getLadderQueryRnd8() : Observable<ladderQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=ladder;year=2019;round=8;source=17').pipe(
        map((data: any) => data.ladder.map((item: any) => new ladderQuery(
          item.mean_rank,
          item.teamid,
          item.percentage,
          item.swarns,
          item.updated,
          item.year,
          item.team,
          item.source,
          item.round,
          item.rank,
          item.sourceid,
          item.wins
        )))
      ); 
    }
    getLadderQueryRnd9() : Observable<ladderQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=ladder;year=2019;round=9;source=17').pipe(
        map((data: any) => data.ladder.map((item: any) => new ladderQuery(
          item.mean_rank,
          item.teamid,
          item.percentage,
          item.swarns,
          item.updated,
          item.year,
          item.team,
          item.source,
          item.round,
          item.rank,
          item.sourceid,
          item.wins
        )))
      ); 
    }
    getLadderQueryRnd10() : Observable<ladderQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=ladder;year=2019;round=10;source=17').pipe(
        map((data: any) => data.ladder.map((item: any) => new ladderQuery(
          item.mean_rank,
          item.teamid,
          item.percentage,
          item.swarns,
          item.updated,
          item.year,
          item.team,
          item.source,
          item.round,
          item.rank,
          item.sourceid,
          item.wins
        )))
      ); 
    }
    

    getRound01() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=1').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound02() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=2').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound03() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=3').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound04() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=4').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound05() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=5').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound06() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=6').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound07() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=7').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound08() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=8').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound09() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=9').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound10() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=10').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound11() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=11').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound12() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=12').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound13() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=13').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound14() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=14').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound15() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=15').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound16() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=16').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound17() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=17').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound18() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=18').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound19() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=19').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound20() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=20').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound21() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=21').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound22() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=22').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound23() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=23').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

    getRound24() : Observable<DataQuery[]> {
      return this.http.get('https://api.squiggle.com.au/?q=games;year=2019;round=24').pipe(
        map((data: any) => data.games.map((item: any) => new DataQuery(
          item.complete,
          item.is_grand_final,
          item.tz,
          item.hbehinds,
          item.ateam,
          item.winnerteamid,
          item.hgoals,
          item.updated,
          item.round,
          item.is_final,
          item.hscore,
          item.abehinds,
          item.winner,
          item.ascore,
          item.hteam,
          item.ateamid,
          item.venue,
          item.hteamid,
          item.agoals,
          item.year,
          item.date,
          item.id
        )))
      ); 
    }

  getRoundData2019(number: any): Observable<DataQuery[]> {
    if(number==1){
      return this.getRound01();
    }
    else if(number ==2){
      return this.getRound02();
    }
    else if(number==3){
      return this.getRound03();
    }
    else if(number==4){
      return this.getRound04();
    }
    else if(number ==5){
      return this.getRound05();
    }
    else if(number==6){
      return this.getRound06();
    }
    else if(number==7){
      return this.getRound07();
    }
    else if(number ==8){
      return this.getRound08();
    }
    else if(number==9){
      return this.getRound09();
    }
    else if(number==10){
      return this.getRound10();
    }
    else if(number==11){
      return this.getRound11();
    }
    else if(number ==12){
      return this.getRound12();
    }
    else if(number==13){
      return this.getRound13();
    }
    else if(number==14){
      return this.getRound14();
    }
    else if(number ==15){
      return this.getRound15();
    }
    else if(number==16){
      return this.getRound16();
    }
    else if(number==17){
      return this.getRound17();
    }
    else if(number ==18){
      return this.getRound18();
    }
    else if(number==19){
      return this.getRound19();
    }
    else if(number==20){
      return this.getRound20();
    }
    else if(number==21){
      return this.getRound21();
    }
    else if(number ==22){
      return this.getRound22();
    }
    else if(number==23){
      return this.getRound23();
    }
    else if(number==24){
      return this.getRound24();
    }
  }
}  

