import { Component, OnInit } from '@angular/core';
import { QueryServices } from '../query-services';
import { Team } from '../team';
import { DataQuery } from '../data-query';
import { ladderQuery } from '../ladderQuery';
import { CompileMetadataResolver } from '@angular/compiler';

@Component({
  selector: 'app-ladder',
  templateUrl: './ladder.component.html',
  styleUrls: ['./ladder.component.css']
})
export class LadderComponent implements OnInit {

  teams: Team[];
  weeks: DataQuery[];
  myVar: DataQuery[];
  myVar2: DataQuery[];
  myVar3: DataQuery[];
  myVar4: DataQuery[];
  chadbonks: ladderQuery[];
  chadbonk2s: ladderQuery[];
  chadbonk3s: ladderQuery[];
  chadbonk4s: ladderQuery[];
  chadbonk5s: ladderQuery[];
  chadbonk6s: ladderQuery[];

  constructor(private queryServices: QueryServices) {

  }
  ngOnInit() {
    this.getLadderQueryRnd1();
    this.getLadderQueryRnd2();
    this.getLadderQueryRnd3();
    this.getLadderQueryRnd4();
    this.getLadderQuery();
    this.getLadderQueryRnd6();
    this.getLadderQueryRnd7();
    this.getLadderQueryRnd8();
    this.getLadderQueryRnd9();
    this.getLadderQueryRnd10();
  }
  getLadderQueryRnd1(): void {
    this.queryServices.getLadderQueryRnd1().subscribe(temp => { this.myVar = temp; });
  }
  getLadderQueryRnd2(): void {
    this.queryServices.getLadderQueryRnd2().subscribe(temp => { this.myVar2 = temp; });
  }
  getLadderQueryRnd3(): void {
    this.queryServices.getLadderQueryRnd3().subscribe(temp => { this.myVar3 = temp; });
  }
  getLadderQueryRnd4(): void {
    this.queryServices.getLadderQueryRnd4().subscribe(temp => { this.myVar4 = temp; });
  }
  getLadderQuery(): void {
    this.queryServices.getLadderQuery().subscribe(temp => { this.chadbonks = temp; });
  }
  getLadderQueryRnd6(): void {
    this.queryServices.getLadderQueryRnd6().subscribe(temp => { this.chadbonk2s = temp; });
  }
  getLadderQueryRnd7(): void {
    this.queryServices.getLadderQueryRnd7().subscribe(temp => { this.chadbonk3s = temp; });
  }
  getLadderQueryRnd8(): void {
    this.queryServices.getLadderQueryRnd8().subscribe(temp => { this.chadbonk4s = temp; });
  }
  getLadderQueryRnd9(): void {
    this.queryServices.getLadderQueryRnd9().subscribe(temp => { this.chadbonk5s = temp; });
  }
  getLadderQueryRnd10(): void {
    this.queryServices.getLadderQueryRnd10().subscribe(temp => { this.chadbonk6s = temp; });
  };
}
